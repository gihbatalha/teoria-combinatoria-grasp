package models;

public enum ConstructiveHeuristicType {
	DEFAULT, REACTIVE_GRASP, RANDOM_PLUS
}
