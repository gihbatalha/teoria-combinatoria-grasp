ALPHA=0.1_FIRST_REACTIVE_GRASP
Best solution: Solution: cost=[-120.0], size=[11], elements=[16, 0, 2, 18, 3, 4, 6, 9, 12, 13, 14]
Time: 0.465seg 

ALPHA=0.1_BEST_REACTIVE_GRASP
Best solution: Solution: cost=[-122.0], size=[11], elements=[16, 0, 2, 18, 3, 4, 8, 9, 12, 13, 14]
Time: 0.492seg 

ALPHA=0.5_FIRST_REACTIVE_GRASP
Best solution: Solution: cost=[-120.0], size=[11], elements=[16, 0, 2, 18, 3, 4, 6, 9, 12, 13, 14]
Time: 0.243seg 

ALPHA=0.5_BEST_REACTIVE_GRASP
Best solution: Solution: cost=[-122.0], size=[11], elements=[16, 0, 2, 18, 3, 4, 8, 9, 12, 13, 14]
Time: 0.477seg 

ALPHA=0.9_FIRST_REACTIVE_GRASP
Best solution: Solution: cost=[-120.0], size=[11], elements=[16, 0, 2, 18, 3, 4, 6, 9, 12, 13, 14]
Time: 0.22seg 

ALPHA=0.9_BEST_REACTIVE_GRASP
Best solution: Solution: cost=[-122.0], size=[11], elements=[16, 0, 2, 18, 3, 4, 8, 9, 12, 13, 14]
Time: 0.368seg 

