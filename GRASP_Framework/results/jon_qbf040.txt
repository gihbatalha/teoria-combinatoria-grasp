ALPHA=0.1_FIRST_DEFAULT
Best solution: Solution: cost=[-308.0], size=[20], elements=[32, 0, 33, 4, 36, 37, 7, 9, 11, 12, 14, 15, 19, 20, 21, 24, 27, 28, 29, 31]
Time: 1.76seg 

ALPHA=0.1_BEST_DEFAULT
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 2.909seg 

ALPHA=0.1_FIRST_RANDOM_PLUS_P_0.1
Best solution: Solution: cost=[-322.0], size=[19], elements=[32, 33, 1, 34, 3, 36, 4, 7, 39, 9, 11, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 1.453seg 

ALPHA=0.1_FIRST_RANDOM_PLUS_P_0.2
Best solution: Solution: cost=[-314.0], size=[19], elements=[32, 0, 33, 4, 36, 7, 9, 11, 12, 14, 15, 19, 20, 21, 24, 27, 28, 29, 31]
Time: 1.003seg 

ALPHA=0.1_FIRST_RANDOM_PLUS_P_0.3
Best solution: Solution: cost=[-309.0], size=[21], elements=[32, 1, 33, 34, 3, 36, 4, 37, 38, 39, 7, 9, 11, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 0.579seg 

ALPHA=0.1_FIRST_RANDOM_PLUS_P_0.1
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 2.831seg 

ALPHA=0.1_FIRST_RANDOM_PLUS_P_0.2
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 2.839seg 

ALPHA=0.1_FIRST_RANDOM_PLUS_P_0.3
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 2.87seg 

ALPHA=0.5_FIRST_DEFAULT
Best solution: Solution: cost=[-301.0], size=[17], elements=[32, 33, 34, 4, 36, 6, 7, 9, 11, 15, 18, 21, 23, 24, 28, 29, 31]
Time: 0.726seg 

ALPHA=0.5_BEST_DEFAULT
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 4.991seg 

ALPHA=0.5_FIRST_RANDOM_PLUS_P_0.1
Best solution: Solution: cost=[-312.0], size=[18], elements=[32, 33, 34, 35, 4, 36, 7, 9, 11, 14, 15, 19, 21, 24, 27, 28, 29, 31]
Time: 1.034seg 

ALPHA=0.5_FIRST_RANDOM_PLUS_P_0.2
Best solution: Solution: cost=[-294.0], size=[19], elements=[32, 33, 1, 34, 35, 4, 36, 37, 7, 9, 10, 15, 19, 21, 22, 24, 28, 29, 31]
Time: 0.848seg 

ALPHA=0.5_FIRST_RANDOM_PLUS_P_0.3
Best solution: Solution: cost=[-288.0], size=[18], elements=[32, 0, 33, 35, 36, 4, 9, 11, 12, 14, 15, 19, 20, 21, 24, 27, 29, 31]
Time: 0.55seg 

ALPHA=0.5_FIRST_RANDOM_PLUS_P_0.1
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 5.88seg 

ALPHA=0.5_FIRST_RANDOM_PLUS_P_0.2
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 5.598seg 

ALPHA=0.5_FIRST_RANDOM_PLUS_P_0.3
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 4.743seg 

ALPHA=0.9_FIRST_DEFAULT
Best solution: Solution: cost=[-145.0], size=[8], elements=[0, 1, 36, 22, 23, 24, 9, 31]
Time: 0.253seg 

ALPHA=0.9_BEST_DEFAULT
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 6.658seg 

ALPHA=0.9_FIRST_RANDOM_PLUS_P_0.1
Best solution: Solution: cost=[-170.0], size=[14], elements=[0, 1, 34, 2, 36, 9, 10, 14, 18, 19, 22, 25, 26, 31]
Time: 0.399seg 

ALPHA=0.9_FIRST_RANDOM_PLUS_P_0.2
Best solution: Solution: cost=[-217.0], size=[18], elements=[0, 32, 33, 1, 34, 35, 36, 4, 7, 11, 12, 13, 15, 19, 24, 27, 28, 31]
Time: 0.416seg 

ALPHA=0.9_FIRST_RANDOM_PLUS_P_0.3
Best solution: Solution: cost=[-207.0], size=[17], elements=[32, 33, 34, 3, 36, 4, 37, 7, 9, 10, 15, 18, 21, 22, 24, 28, 29]
Time: 0.43seg 

ALPHA=0.9_FIRST_RANDOM_PLUS_P_0.1
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 5.114seg 

ALPHA=0.9_FIRST_RANDOM_PLUS_P_0.2
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 9.235seg 

ALPHA=0.9_FIRST_RANDOM_PLUS_P_0.3
Best solution: Solution: cost=[-326.0], size=[18], elements=[32, 33, 34, 4, 36, 38, 7, 9, 11, 14, 15, 19, 21, 23, 24, 28, 29, 31]
Time: 6.151seg 

